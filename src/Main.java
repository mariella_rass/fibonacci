import java.util.Scanner;
/**
 * Created by mary on 17.04.15.
 */
public class Main {

    public static void main(String args[]) {
        System.out.print("Enter your number from Fibonacci sequance: ");
        //Enter your number (not index) from Fibonacci sequence there to get sum for even-evaluated
        //elements upper to that number
        long number = new Scanner(System.in).nextInt();
        long sum = 0;
        System.out.println("Fibonacci even-evaluated elements: ");

        for (int i = 3; ; i+=3) {
            long value = fibonacci(i);
            if (value > number) {
                break;
            }
                sum += value;
                System.out.println("Fibo # " + i + ": " + value);

        }
        System.out.print("sum: " + sum + " "); //sum of even-evaluated numbers
    }

    public static long fibonacci(long i) {
        if (i < 3) {
            return 1;
        }
        long number = i/2;
        if (i % 2 == 0) {
            return (long) Math.abs(Math.pow(fibonacci(number+1), 2) - Math.pow(fibonacci(number - 1), 2));
            // We using the property of Fibonacci sequence for even-evaluated elements
            // to get more details please read:  http://www.trinitas.ru/rus/doc/0232/009a/1094-rfib.pdf
        }
        else {
            return (long) (Math.pow(((double)fibonacci(number)), 2) + Math.pow(((double)fibonacci(number + 1)), 2));

        }
    }

    }

